#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <cmath>

#define PI 3.14159
using namespace std;

int main()
{

	cout << "\n\t Imprimiendo por consola, Hola yitodeveloper!" << endl; // Para imprimir por consola se debe utilizar el objeto cout

	// << -> operador de insercion
	// cout = console output
	// endl = salto de linea

	//Secuencias de escape
	/*
		\n -> salto de linea
		\b -> retroceso
		\r -> retorno del carro
		\t -> tabulador
	*/

	// Variables
	int a = 7e4; // Enteros (Se le asigna un valor con notacion cientifica con el fin de describir numeros grandes de una manera simplificada
	string s = "hola";
	char b = 'a'; // Caracter
	float c = 2.4; // Flotantes
	double d = 2.4566; // Double Decimal
	bool e = true; // Booleano

	cout << a << endl; // Imprimir una variable

	// Tipo de Variables
	// char peso 1 Bytes -> 256 caracteres posibles -> Rango de -128 a 127
	char caracter = -128;
	cout << caracter << endl;
	// int peso 4 Bytes (variable) -> 2^32 valores posibles -> Rango de -2147483648 a 2147483647
	// 32 bits en total, donde 1 esta destinado al signo y el resto a 31 bits
	// Si se utiliza unsigned se utilizan los 32 bits para los valores 4294967296
	int entero = -1;
	unsigned int enteroSinSigno = -1;

	cout << sizeof(enteroSinSigno) << endl; // La funcion size of permite obtener el tama�o de una variable en memoria
	// bool peso 1 byte

	// float peso 4 bytes, rango 1.17 e-38 a 3.40e38

	// double peso 8 bytes , rango 2.22e-308 a 1.80e308

	int valorConsola;

	cin >> valorConsola; // cin = Console input

	cout << "El valor ingresado es : " << valorConsola << endl;

	// Operadores Matematicos
	/*
		+ -> Suma
		- -> Resta
		* -> Multiplicacion
		/ -> Division
		% -> Modulo (Solo se puede realizar con variables de tipo entero
	*/

	// Para efectos de la division cuando los dos valores a dividir son enteros, el cuociente da por resultado la parte entera de la divisi�n
	// En el caso de que uno de los valores a dividir sea float o double, la divisi�n entrega los decimales, realizando una divisi�n real
	
	// Operadores Logicos
	/*
		&& -> Y
		|| -> O
		! -> No
	*/

	// Comparadores
	/*
		== -> Igualdad
		!= -> Diferente
		< -> Menor
		> -> Mayor
		<= -> Menor o igual
		>= -> Mayor o Igual

	*/
	
	// Libreria Cmath para operaciones matematicas complejas
	/*
		pow(base,exponente) -> Potencia
		sqrt(numero) -> Raiz Cuadrada
		sin(*valor) -> Seno                     *valor = El valor proporcionado debe ser en hexagecimales
		cos(*valor) -> Coseno
		tan(*valor) -> tangente
	*/
	
	// Declaraci�n de constantes
	const double g = -9.58;
	system("PAUSE");

	return 0;
}